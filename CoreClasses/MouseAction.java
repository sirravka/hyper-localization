package org.hyper.inventory.api;

import lombok.NonNull;

import java.util.Arrays;

public enum MouseAction {
    LEFT, SHIFT_LEFT, RIGHT, SHIFT_RIGHT, WINDOW_BORDER_LEFT, WINDOW_BORDER_RIGHT, MIDDLE, NUMBER_KEY, DOUBLE_CLICK, DROP, CONTROL_DROP, CREATIVE, UNKNOWN;

    public static final MouseAction[] MOUSE_ACTIONS;

    static {
        MOUSE_ACTIONS = values();
    }

    public static MouseAction getMouseAction(@NonNull String actionName) {
        if (actionName == null)
            throw new NullPointerException("actionName is marked non-null but is null");
        return Arrays.<MouseAction>stream(MOUSE_ACTIONS).filter(mouseAction -> mouseAction.name().equalsIgnoreCase(actionName))
                .findFirst().orElse(null);
    }
}

package org.hyper.api.utility;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import lombok.NonNull;

public final class DateUtil {
    private DateUtil() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    public static final Date DATE_FORMATTER = new Date();

    public static final String DEFAULT_DATETIME_PATTERN = "dd.MM.yyyy h:mm:ss a";

    public static final String DEFAULT_DATE_PATTERN = "EEE, d MMM, yyyy";

    public static final String DEFAULT_TIME_PATTERN = "h:mm a";

    public static String formatPattern(@NonNull String pattern) {
        if (pattern == null)
            throw new NullPointerException("pattern is marked non-null but is null");
        return createDateFormat(pattern).format(DATE_FORMATTER);
    }

    public static String formatTime(long millis, @NonNull String pattern) {
        if (pattern == null)
            throw new NullPointerException("pattern is marked non-null but is null");
        return createDateFormat(pattern).format(new Time(millis));
    }

    private static DateFormat createDateFormat(@NonNull String pattern) {
        if (pattern == null)
            throw new NullPointerException("pattern is marked non-null but is null");
        return new SimpleDateFormat(pattern);
    }

    public static Date parseDate(@NonNull String datePattern, @NonNull String formattedDate) throws ParseException {
        try {
            if (datePattern == null)
                throw new NullPointerException("datePattern is marked non-null but is null");
            if (formattedDate == null)
                throw new NullPointerException("formattedDate is marked non-null but is null");
            return createDateFormat(datePattern).parse(formattedDate);
        } catch (Throwable $ex) {
            throw $ex;
        }
    }
}

package org.hyper.utility.api.mojang;

import java.util.concurrent.TimeUnit;

public class MojangSkin {
    private final String skinName;

    private final String playerUUID;

    private final String value;

    private final String signature;

    private final long timestamp;

    public MojangSkin(String skinName, String playerUUID, String value, String signature, long timestamp) {
        this.skinName = skinName;
        this.playerUUID = playerUUID;
        this.value = value;
        this.signature = signature;
        this.timestamp = timestamp;
    }

    public String getSkinName() {
        return this.skinName;
    }

    public String getPlayerUUID() {
        return this.playerUUID;
    }

    public String getValue() {
        return this.value;
    }

    public String getSignature() {
        return this.signature;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public boolean isExpired() {
        return (System.currentTimeMillis() - this.timestamp > TimeUnit.HOURS.toMillis(5L));
    }
}
